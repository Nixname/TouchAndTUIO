use crate::iterators::SlotFinder;

#[test]
fn slot_finder() {
    {
        let slots = vec![0];

        let mut slot_finder = SlotFinder::new(slots.iter().peekable());

        assert_eq!(slot_finder.next(), Some(1));
        assert_eq!(slot_finder.next(), Some(2));
    }
    {
        let slots = vec![1];

        let mut slot_finder = SlotFinder::new(slots.iter().peekable());

        assert_eq!(slot_finder.next(), Some(0));
        assert_eq!(slot_finder.next(), Some(2));
    }
    {
        let slots = vec![];

        let mut slot_finder = SlotFinder::new(slots.iter().peekable());

        assert_eq!(slot_finder.next(), Some(0));
        assert_eq!(slot_finder.next(), Some(1));
    }
    {
        let slots = vec![1, 2, 5];

        let mut slot_finder = SlotFinder::new(slots.iter().peekable());

        assert_eq!(slot_finder.next(), Some(0));
        assert_eq!(slot_finder.next(), Some(3));
        assert_eq!(slot_finder.next(), Some(4));
        assert_eq!(slot_finder.next(), Some(6));
        assert_eq!(slot_finder.next(), Some(7));
    }
}
