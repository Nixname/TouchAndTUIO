use std::collections::{BTreeMap, HashMap};
use std::fs;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::sync::RwLock;

use evdev::{AbsoluteAxisType, Device, InputEventKind, Synchronization};
use helpers::check_access_to_file;
use nix::unistd::getgroups;
use regex::Regex;

use rayon::prelude::*;

use crate::variables::{get_constants, Rotation};
use tuio::mouse_data::{CursorData, MouseData};

#[derive(Debug)]
struct CursorState {
    pub tracking_id: i32,
    pub abs_x: i32,
    pub abs_y: i32,
}

#[derive(Debug)]
struct MouseState {
    pub slots: HashMap<u16, CursorState>,
}

#[derive(Debug)]
struct EventMouseLimits {
    pub min_abs_x: i32,
    pub max_abs_x: i32,
    pub min_abs_y: i32,
    pub max_abs_y: i32,
}

/// event_id, event_path, event_file_name (without path), device_name
pub fn get_event_name_mapping() -> Result<BTreeMap<u16, (PathBuf, String, String)>, String> {
    let dev_input_dir = Path::new("/dev/input");

    if !dev_input_dir.is_dir() {
        return Err(format!(
            "The directory '{}' does not exist.",
            dev_input_dir.to_str().ok_or_else(|| {
                "The directory path cannot be converted to proper unicode.".to_string()
            })?
        ));
    }

    let entries: Vec<(PathBuf, String, u16)> = {
        let mut entries: Vec<Result<(PathBuf, String), String>> = fs::read_dir(dev_input_dir)
            .map_err(|error| error.to_string())?
            .map(|entry| {
                let entry = entry.map_err(|error| error.to_string())?;
                let file_name = entry.file_name().into_string().map_err(|_| {
                    "The file name cannot be converted to proper unicode.".to_string()
                })?;

                Ok((entry.path(), file_name))
            })
            .collect();

        if entries.iter().any(|result| result.is_err()) {
            return Err(entries
                .iter_mut()
                .filter_map(|result| result.as_mut().err())
                .reduce(|first, second| {
                    first.push('\n');
                    first.push_str(second);
                    first
                })
                .unwrap()
                .clone());
        }

        let regex = Regex::new(r"^event(\d+)$")
            .expect("The regex for capturing the number of /dev/input/event* does not compile");

        entries
            .drain(..)
            .filter_map::<(PathBuf, String, u16), _>(|result| {
                let (entry, file_name) = result.expect("There shouldn't be any Err(_) here.");

                match regex.captures(&file_name) {
                    None => None,
                    Some(captures) => {
                        if captures.len() > 2 {
                            None
                        } else {
                            let capture = captures.get(1).unwrap();
                            let event_number = capture
                                .as_str()
                                .parse::<u16>()
                                .expect("Couldn't read the number of an event.");

                            Some((entry, file_name, event_number))
                        }
                    }
                }
            })
            .collect()
    };

    {
        let member_groups = getgroups().map_err(|error| error.to_string())?;

        for (event_path, _file_name, _event_number) in &entries {
            check_access_to_file(event_path, Some(&member_groups))?;
        }
    }

    let event_to_name_mapping =
        RwLock::<BTreeMap<u16, (PathBuf, String, String)>>::new(BTreeMap::new());

    entries
        .par_iter()
        .for_each(|(entry, file_name, event_number)| {
            let device =
                Device::open(entry).unwrap_or_else(|error| panic!("{}", error.to_string()));

            let name = device.name().unwrap_or_else(|| {
                panic!("Cannot find the name for device '{}'.", entry.display())
            });

            let mut write_lock = event_to_name_mapping
                .write()
                .expect("The RwLock is poisoned.");

            write_lock.insert(
                *event_number,
                (entry.clone(), file_name.clone(), name.to_string()),
            );
        });

    let read_lock = event_to_name_mapping
        .read()
        .expect("Some thread has paniced when trying to read a devices name.");

    Ok((*read_lock).clone())
}

pub fn read_mouse_data(
    sender: crossbeam_channel::Sender<MouseData>,
    event_path: &Path,
) -> Result<(), String> {
    check_access_to_file(event_path, None)?;

    let constants = get_constants().map_err(|_| "Cannot read the constants".to_string())?;

    let mut device = Device::open(event_path).map_err(|error| {
        format!(
            "Cannot open device: {}\nError: {:#?}",
            event_path.display(),
            error
        )
    })?;

    let event_mouse_limits: EventMouseLimits = {
        let abs_state = device
            .get_abs_state()
            .map_err(|_| "The current state of the device cannot be fetched.")?;

        let abs_info = abs_state
            .get(AbsoluteAxisType::ABS_X.0 as usize)
            .ok_or("Cannot access the limits in x direction.")?;
        let min_abs_x = abs_info.minimum;
        let max_abs_x = abs_info.maximum;

        let abs_info = abs_state
            .get(AbsoluteAxisType::ABS_X.0 as usize)
            .ok_or("Cannot access the limits in y direction.")?;
        let min_abs_y = abs_info.minimum;
        let max_abs_y = abs_info.maximum;

        EventMouseLimits {
            min_abs_x,
            max_abs_x,
            min_abs_y,
            max_abs_y,
        }
    };

    if constants.block_event {
        device
            .grab()
            .map_err(|_| format!("Couldn't grab device '{}'", event_path.display()))?;
    }

    let mut mouse_state = MouseState {
        slots: HashMap::new(),
    };

    let mut cur_slot: u16 = 0;

    loop {
        let fetched_events = device
            .fetch_events()
            .map_err(|error| format!("Could not fetch any further events.\n{:#?}", error))?;

        for fetched_event in fetched_events {
            match fetched_event.kind() {
                InputEventKind::AbsAxis(abs_axis_type) => {
                    fn ensure_slot_exists(mouse_state: &mut MouseState, cur_slot: u16) {
                        mouse_state.slots.entry(cur_slot).or_insert(CursorState {
                            tracking_id: 0,
                            abs_x: 0,
                            abs_y: 0,
                        });
                    }
                    fn get_cursor_state(
                        mouse_state: &mut MouseState,
                        cur_slot: u16,
                    ) -> &mut CursorState {
                        mouse_state.slots.get_mut(&cur_slot).unwrap()
                    }

                    match abs_axis_type {
                        AbsoluteAxisType::ABS_MT_SLOT => {
                            cur_slot = fetched_event.value() as u16;
                            ensure_slot_exists(&mut mouse_state, cur_slot);
                        }
                        AbsoluteAxisType::ABS_MT_POSITION_X => {
                            ensure_slot_exists(&mut mouse_state, cur_slot);
                            get_cursor_state(&mut mouse_state, cur_slot).abs_x =
                                fetched_event.value();
                        }
                        AbsoluteAxisType::ABS_MT_POSITION_Y => {
                            ensure_slot_exists(&mut mouse_state, cur_slot);
                            get_cursor_state(&mut mouse_state, cur_slot).abs_y =
                                fetched_event.value();
                        }
                        AbsoluteAxisType::ABS_MT_TRACKING_ID => {
                            if fetched_event.value() == -1 {
                                mouse_state.slots.remove(&cur_slot);
                            } else {
                                ensure_slot_exists(&mut mouse_state, cur_slot);
                                get_cursor_state(&mut mouse_state, cur_slot).tracking_id =
                                    fetched_event.value();
                            }
                        }
                        _ => {}
                    }
                }
                InputEventKind::Synchronization(Synchronization::SYN_REPORT) => {
                    sender
                        .send(MouseData {
                            timestamp: fetched_event.timestamp(),
                            cursors: HashMap::from_iter(mouse_state.slots.values().map(
                                |cursor_state| {
                                    let current_x = if !constants.mirror_x {
                                        cursor_state.abs_x - event_mouse_limits.min_abs_x
                                    } else {
                                        event_mouse_limits.max_abs_x - cursor_state.abs_x
                                    };
                                    let current_y = if !constants.mirror_y {
                                        cursor_state.abs_y - event_mouse_limits.min_abs_y
                                    } else {
                                        event_mouse_limits.max_abs_y - cursor_state.abs_y
                                    };

                                    let normalised_x = current_x as f32
                                        / (event_mouse_limits.max_abs_x
                                            - event_mouse_limits.min_abs_x)
                                            as f32;

                                    let normalised_y = current_y as f32
                                        / (event_mouse_limits.max_abs_y
                                            - event_mouse_limits.min_abs_y)
                                            as f32;

                                    let (resulting_x, resulting_y) = match constants.rotation {
                                        Rotation::Top => (normalised_x, normalised_y),
                                        Rotation::Right => (1. - normalised_y, normalised_x),
                                        Rotation::Bottom => (1. - normalised_x, 1. - normalised_y),
                                        Rotation::Left => (normalised_y, 1. - normalised_x),
                                    };

                                    (
                                        cursor_state.tracking_id as u16,
                                        CursorData {
                                            x: resulting_x,
                                            y: resulting_y,
                                        },
                                    )
                                },
                            )),
                        })
                        .unwrap();
                }
                _ => {}
            }
        }
    }
}
