use std::{
    fs,
    os::unix::prelude::{MetadataExt, PermissionsExt},
    path::Path,
};

use nix::unistd::{getgroups, Gid, Group, Uid};

pub fn check_access_to_file(
    file_path: &Path,
    member_groups: Option<&Vec<Gid>>,
) -> Result<(), String> {
    if Uid::effective().is_root() {
        return Ok(());
    }

    let metadata = fs::metadata(file_path).map_err(|error| {
        format!(
            "Cannot open file '{}'.\n{:#?}",
            file_path.display(),
            error.to_string()
        )
    })?;
    let permissions = metadata.permissions();
    #[allow(clippy::unusual_byte_groupings)]
    let file_mode = permissions.mode() & 0b111_111_111;

    #[allow(clippy::unusual_byte_groupings)]
    if ((file_mode & 0b110_000_000) == 0b110_000_000) && metadata.uid() == Uid::effective().as_raw()
    {
        return Ok(());
    }

    #[allow(clippy::unusual_byte_groupings)]
    if (file_mode & 0b000_000_110) == 0b000_000_110 {
        return Ok(());
    }

    #[allow(clippy::unusual_byte_groupings)]
    if (file_mode & 0b000_110_000) != 0b000_110_000 {
        return Err(format!(
            "Cannot access file '{}'. Execute this command with super user privileges.",
            file_path.display()
        ));
    }

    let temp_member_groups = match member_groups {
        None => Some(getgroups().map_err(|error| error.to_string())?),
        Some(_) => None,
    };
    let member_groups = member_groups.unwrap_or_else(|| temp_member_groups.as_ref().unwrap());

    let file_group = metadata.gid();

    if !member_groups
        .iter()
        .any(|group| group.as_raw() == file_group)
    {
        let group_info = Group::from_gid(Gid::from_raw(file_group))
            .map_err(|e| e.to_string())?
            .ok_or_else(|| "No corresponding group found.".to_owned())?;
        return Err(format!("The current user is in not in the group '{}', which would allow him to access the file '{}'. Add him to this group or execute this command with super user privileges.", group_info.name, file_path.display()));
    }

    Ok(())
}
