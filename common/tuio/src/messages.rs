use std::{net::SocketAddr, sync::atomic};

use rosc::{OscBundle, OscMessage, OscPacket, OscTime, OscType};

use crate::{
    mouse_data::{ComputedMouseData, MouseData},
    types::SourceIdentifier,
};

pub const PROFILE_NAME_2DCUR: &str = "/tuio/2Dcur";

pub trait TuioMessage: Sized {
    fn to_osc(&self) -> OscMessage;
    fn from_osc(address_prefix: &str, osc: &OscMessage) -> Result<Self, String>;
}

pub enum TuioMessageEnum {
    SourceMessage(SourceMessage),
    AliveMessage(AliveMessage),
    SetMessage(SetMessage),
    FSeqMessage(FSeqMessage),
}

#[derive(Debug, Eq, PartialEq)]
pub struct SourceMessage {
    pub address_prefix: String,
    pub source: String,
}
impl TuioMessage for SourceMessage {
    fn to_osc(&self) -> OscMessage {
        OscMessage {
            addr: format!("{}{}", self.address_prefix, PROFILE_NAME_2DCUR),
            args: vec![
                OscType::String("source".to_string()),
                OscType::String(self.source.clone()),
            ],
        }
    }

    fn from_osc(address_prefix: &str, osc: &OscMessage) -> Result<Self, String> {
        check_address(osc, address_prefix)?;

        if osc.args.len() != 2 {
            return Err(format!("Unexpected number of arguments.\n{:#?}", osc.args));
        }

        check_first_message_part(osc, "source")?;

        let source = if let OscType::String(arg1) = &osc.args[1] {
            arg1
        } else {
            return Err(format!(
                "The second argument is not a string:\n{:#?}",
                osc.args[1]
            ));
        };

        Ok(Self {
            address_prefix: address_prefix.to_owned(),
            source: source.to_owned(),
        })
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct AliveMessage {
    pub address_prefix: String,
    pub session_ids: Vec<u16>,
}
impl TuioMessage for AliveMessage {
    fn to_osc(&self) -> OscMessage {
        let mut args = vec![OscType::String("alive".to_string())];

        args.extend(
            self.session_ids
                .iter()
                .map(|session_id| OscType::Int(*session_id as i32)),
        );

        OscMessage {
            addr: format!("{}{}", self.address_prefix, PROFILE_NAME_2DCUR),
            args,
        }
    }

    fn from_osc(address_prefix: &str, osc: &OscMessage) -> Result<Self, String> {
        check_address(osc, address_prefix)?;

        if osc.args.is_empty() {
            return Err("There were no arguments contained".to_owned());
        }

        check_first_message_part(osc, "alive")?;

        let mut session_ids: Vec<u16> = Vec::with_capacity(osc.args.len() - 1);
        for (index, argument) in osc.args[1..].iter().enumerate() {
            if let OscType::Int(id) = argument {
                session_ids.push(*id as u16);
            } else {
                return Err(format!(
                    "Argument with index '{}' is not an integer.\nArgument: {:#?}",
                    index + 1,
                    argument
                ));
            }
        }

        Ok(Self {
            address_prefix: address_prefix.to_owned(),
            session_ids,
        })
    }
}

#[derive(Debug, PartialEq)]
pub struct SetMessage {
    pub address_prefix: String,
    pub session_id: u16,
    pub x: f32,
    pub y: f32,
    pub x_velocity: f32,
    pub y_velocity: f32,
    pub motion_acceleration: f32,
}
impl TuioMessage for SetMessage {
    fn to_osc(&self) -> OscMessage {
        OscMessage {
            addr: format!("{}{}", self.address_prefix, PROFILE_NAME_2DCUR),
            args: vec![
                OscType::String("set".to_string()),
                OscType::Int(self.session_id as i32),
                OscType::Float(self.x),
                OscType::Float(self.y),
                OscType::Float(self.x_velocity),
                OscType::Float(self.y_velocity),
                OscType::Float(self.motion_acceleration),
            ],
        }
    }

    fn from_osc(address_prefix: &str, osc: &OscMessage) -> Result<Self, String> {
        check_address(osc, address_prefix)?;

        if osc.args.len() != 7 {
            return Err(format!(
                "The message does not contain the expected number of arguments.\nActual: {}",
                osc.args.len()
            ));
        }

        check_first_message_part(osc, "set")?;

        let session_id = extract_argument_integer(&osc.args, 1)? as u16;
        let x = extract_argument_float(&osc.args, 2)?;
        let y = extract_argument_float(&osc.args, 3)?;
        let x_velocity = extract_argument_float(&osc.args, 4)?;
        let y_velocity = extract_argument_float(&osc.args, 5)?;
        let motion_acceleration = extract_argument_float(&osc.args, 6)?;

        Ok(Self {
            address_prefix: address_prefix.to_owned(),
            session_id,
            x,
            y,
            x_velocity,
            y_velocity,
            motion_acceleration,
        })
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct FSeqMessage {
    pub address_prefix: String,
    pub sequence: i32,
}
impl FSeqMessage {
    pub fn new(fseq_atomic: &atomic::AtomicU16, address_prefix: &str) -> Self {
        Self {
            sequence: fseq_atomic.fetch_add(1, atomic::Ordering::Relaxed).into(),
            address_prefix: address_prefix.to_string(),
        }
    }
    pub fn keep_alive(address_prefix: &str) -> Self {
        Self {
            sequence: -1,
            address_prefix: address_prefix.to_string(),
        }
    }
}
impl TuioMessage for FSeqMessage {
    fn to_osc(&self) -> OscMessage {
        OscMessage {
            addr: format!("{}{}", self.address_prefix, PROFILE_NAME_2DCUR),
            args: vec![
                OscType::String("fseq".to_string()),
                OscType::Int(self.sequence),
            ],
        }
    }

    fn from_osc(address_prefix: &str, osc: &OscMessage) -> Result<Self, String> {
        check_address(osc, address_prefix)?;

        if osc.args.len() != 2 {
            return Err(format!(
                "The message does not contain the expected number of arguments.\nArguments: {:#?}",
                osc.args
            ));
        }

        check_first_message_part(osc, "fseq")?;

        let sequence = extract_argument_integer(&osc.args, 1)?;

        Ok(Self {
            address_prefix: address_prefix.to_owned(),
            sequence,
        })
    }
}

#[derive(Debug, PartialEq)]
pub struct TuioBundle {
    pub source_message: Option<SourceMessage>,
    pub alive_message: AliveMessage,
    pub set_messages: Vec<SetMessage>,
    pub fseq_message: FSeqMessage,
}
impl TuioBundle {
    pub fn to_osc(&self) -> OscBundle {
        let mut content = Vec::<OscPacket>::with_capacity(
            2 + (if self.source_message.is_some() { 1 } else { 0 }) + self.set_messages.len(),
        );

        if let Some(source_message) = &self.source_message {
            content.push(OscPacket::Message(source_message.to_osc()));
        }

        content.push(OscPacket::Message(self.alive_message.to_osc()));

        self.set_messages
            .iter()
            .for_each(|set_message| content.push(OscPacket::Message(set_message.to_osc())));

        content.push(OscPacket::Message(self.fseq_message.to_osc()));

        OscBundle {
            timetag: OscTime {
                // Special value meaning it should be executed immediately
                seconds: 0,
                fractional: 1,
            },
            content,
        }
    }

    pub fn from_osc(address_prefix: &str, osc: &OscBundle) -> Result<Self, String> {
        if osc.content.len() < 2 {
            return Err(format!("This is no valid TUIO bundle as is contains less than 2 packets.\nNumber of packets: {}", osc.content.len()));
        }

        let mut source_message: Option<SourceMessage> = None;
        let mut alive_message: Option<AliveMessage> = None;
        let mut set_messages = Vec::<SetMessage>::with_capacity(osc.content.len() - 2);
        let mut fseq_message: Option<FSeqMessage> = None;

        #[derive(Debug, Eq, PartialEq)]
        enum BundleMessageStep {
            Source,
            Alive,
            Set,
            FSeq,
            Done,
        }
        impl BundleMessageStep {
            pub fn first() -> Self {
                Self::Source
            }
            pub fn next(&mut self) {
                match *self {
                    Self::Source => *self = Self::Alive,
                    Self::Alive => *self = Self::Set,
                    Self::Set => *self = Self::FSeq,
                    Self::FSeq => *self = Self::Done,
                    Self::Done => *self = Self::Done,
                }
            }
        }

        let penultimate_index = osc.content.len() - 2;
        let mut current_bundle_message_step = BundleMessageStep::first();
        for (index, osc_packet) in osc.content.iter().enumerate() {
            match osc_packet {
                OscPacket::Bundle(_) => {
                    return Err(format!("OscPacket with index '{}' is a bundle.", index))
                }
                OscPacket::Message(osc_message) => loop {
                    match current_bundle_message_step {
                        BundleMessageStep::Source => {
                            match SourceMessage::from_osc(address_prefix, osc_message) {
                                Ok(parsed_source_message) => {
                                    source_message = Some(parsed_source_message);
                                    current_bundle_message_step.next();
                                }
                                Err(_) => {
                                    current_bundle_message_step.next();
                                    continue;
                                }
                            }
                        }
                        BundleMessageStep::Alive => {
                            alive_message =
                                Some(AliveMessage::from_osc(address_prefix, osc_message)?);

                            if index == penultimate_index {
                                current_bundle_message_step = BundleMessageStep::FSeq;
                            } else {
                                current_bundle_message_step.next();
                            }
                        }
                        BundleMessageStep::Set => {
                            set_messages.push(SetMessage::from_osc(address_prefix, osc_message)?);

                            if index == penultimate_index {
                                current_bundle_message_step = BundleMessageStep::FSeq;
                            }
                        }
                        BundleMessageStep::FSeq => {
                            fseq_message =
                                Some(FSeqMessage::from_osc(address_prefix, osc_message)?);
                            current_bundle_message_step.next();
                        }
                        BundleMessageStep::Done => {
                            return Err(format!(
                                "Received more messages than expected.\nAcutal state: {:#?}",
                                current_bundle_message_step
                            ));
                        }
                    }

                    break;
                },
            }
        }

        Ok(Self {
            source_message,
            alive_message: alive_message
                .ok_or_else(|| "No 'alive' message is contained in the bundle.".to_owned())?,
            set_messages,
            fseq_message: fseq_message
                .ok_or_else(|| "No 'fseq' message is contained in the bundle.".to_owned())?,
        })
    }
}

fn check_address(osc_message: &OscMessage, address_prefix: &str) -> Result<(), String> {
    let expected_address = address_prefix.to_owned() + PROFILE_NAME_2DCUR;
    if osc_message.addr != expected_address {
        return Err(format!(
            "The message's address does not match the expectation.\nActual: {}\nExpected: {}",
            osc_message.addr, expected_address
        ));
    }

    Ok(())
}

fn check_first_message_part(osc_message: &OscMessage, expected_arg: &str) -> Result<(), String> {
    if let OscType::String(arg0) = &osc_message.args[0] {
        if arg0 != expected_arg {
            return Err(format!("The first argument indicates a different type of message than the expectation.\nActual: {}\nExpected: {}", arg0, expected_arg));
        }
    } else {
        return Err(format!(
            "The first argument is not a string:\n{:#?}",
            osc_message.args[0]
        ));
    }

    Ok(())
}

fn extract_argument_integer(osc_types: &[OscType], index: usize) -> Result<i32, String> {
    if let OscType::Int(integer) = osc_types.get(index).ok_or_else(|| {
        format!(
            "The index '{}' is out of bounds.\nLength: {}",
            index,
            osc_types.len()
        )
    })? {
        Ok(*integer)
    } else {
        Err(format!(
            "Argument with index '{}' is not an integer.\nArgument: {:#?}",
            index, osc_types[index]
        ))
    }
}

fn extract_argument_float(osc_types: &[OscType], index: usize) -> Result<f32, String> {
    if let OscType::Float(float) = osc_types.get(index).ok_or_else(|| {
        format!(
            "The index '{}' is out of bounds.\nLength: {}",
            index,
            osc_types.len()
        )
    })? {
        Ok(*float)
    } else {
        Err(format!(
            "Argument with index '{}' is not a float.\nArgument: {:#?}",
            index, osc_types[index]
        ))
    }
}

pub fn get_source_message(
    source_identifier: &Option<SourceIdentifier>,
    address_prefix: &str,
    ip_address: &SocketAddr,
) -> Option<SourceMessage> {
    source_identifier
        .as_ref()
        .map(|source_identifier| SourceMessage {
            address_prefix: address_prefix.to_string(),
            source: source_identifier.format(ip_address),
        })
}

pub fn get_alive_message(address_prefix: &str, mouse_data: &MouseData) -> AliveMessage {
    AliveMessage {
        address_prefix: address_prefix.to_string(),
        session_ids: mouse_data.cursors.keys().copied().collect(),
    }
}

pub fn get_set_messages(
    address_prefix: &str,
    mouse_data: &MouseData,
    previous_mouse_data: &Option<ComputedMouseData>,
) -> (ComputedMouseData, Vec<SetMessage>) {
    let computed_mouse_data = ComputedMouseData::new(mouse_data, previous_mouse_data);

    let message_vector = computed_mouse_data
        .cursors
        .iter()
        .map(|(cursor_id, computed_cursor)| {
            let mut x_velocity = 0.;
            let mut y_velocity = 0.;
            let mut motion_acceleration = 0.;

            if let Some(computed_part) = &computed_cursor.computed_part_of_cursor_data {
                x_velocity = computed_part.x_velocity;
                y_velocity = computed_part.y_velocity;

                if let Some(given_motion_acceleration) = computed_part.motion_acceleration {
                    motion_acceleration = given_motion_acceleration;
                }
            }

            SetMessage {
                address_prefix: address_prefix.to_string(),
                session_id: *cursor_id,
                x: computed_cursor.cursor_data.x,
                y: computed_cursor.cursor_data.y,
                x_velocity,
                y_velocity,
                motion_acceleration,
            }
        })
        .collect();

    (computed_mouse_data, message_vector)
}

pub fn get_cursor_bundle(
    address_prefix: &str,
    source_identifier: &Option<SourceIdentifier>,
    fseq_atomic: &atomic::AtomicU16,
    mouse_data: &MouseData,
    previous_mouse_data: &Option<ComputedMouseData>,
    ip_address: &SocketAddr,
) -> (ComputedMouseData, TuioBundle) {
    let (computed_mouse_data, stored_set_messages) =
        get_set_messages(address_prefix, mouse_data, previous_mouse_data);

    (
        computed_mouse_data,
        TuioBundle {
            source_message: get_source_message(source_identifier, address_prefix, ip_address),
            alive_message: get_alive_message(address_prefix, mouse_data),
            set_messages: stored_set_messages,
            fseq_message: FSeqMessage::new(fseq_atomic, address_prefix),
        },
    )
}

pub fn get_keep_alive_bundle(
    address_prefix: &str,
    source_identifier: &Option<SourceIdentifier>,
    mouse_data: &MouseData,
    ip_address: &SocketAddr,
) -> TuioBundle {
    TuioBundle {
        source_message: get_source_message(source_identifier, address_prefix, ip_address),
        alive_message: get_alive_message(address_prefix, mouse_data),
        set_messages: vec![],
        fseq_message: FSeqMessage::keep_alive(address_prefix),
    }
}
