use rosc::{OscBundle, OscMessage, OscPacket, OscTime, OscType};

use crate::messages::{
    AliveMessage, FSeqMessage, SetMessage, SourceMessage, TuioBundle, TuioMessage,
};

#[test]
fn source_message_to_osc() {
    let source_message = SourceMessage {
        address_prefix: "".to_string(),
        source: "".to_string(),
    };

    let converted = source_message.to_osc();

    let expected = OscMessage {
        addr: "/tuio/2Dcur".to_string(),
        args: vec![
            OscType::String("source".to_string()),
            OscType::String("".to_string()),
        ],
    };

    assert_eq!(converted, expected);

    let converted_back = SourceMessage::from_osc("", &converted).unwrap();

    assert_eq!(converted_back, source_message);
}

#[test]
fn alive_message_to_osc() {
    let alive_message = AliveMessage {
        address_prefix: "".to_string(),
        session_ids: vec![16, 17],
    };

    let converted = alive_message.to_osc();

    let expected = OscMessage {
        addr: "/tuio/2Dcur".to_string(),
        args: vec![
            OscType::String("alive".to_string()),
            OscType::Int(16),
            OscType::Int(17),
        ],
    };

    assert_eq!(converted, expected);

    let converted_back = AliveMessage::from_osc("", &converted).unwrap();

    assert_eq!(converted_back, alive_message);
}

#[test]
fn set_message_to_osc() {
    let set_message = SetMessage {
        address_prefix: "".to_string(),
        session_id: 203,
        x: 0.7,
        y: 0.5,
        x_velocity: 4.,
        y_velocity: -0.2,
        motion_acceleration: 7.,
    };

    let converted = set_message.to_osc();

    let expected = OscMessage {
        addr: "/tuio/2Dcur".to_string(),
        args: vec![
            OscType::String("set".to_string()),
            OscType::Int(203),
            OscType::Float(0.7),
            OscType::Float(0.5),
            OscType::Float(4.),
            OscType::Float(-0.2),
            OscType::Float(7.),
        ],
    };

    assert_eq!(converted, expected);

    let converted_back = SetMessage::from_osc("", &converted).unwrap();

    assert_eq!(converted_back, set_message);
}

#[test]
fn fseq_message_to_osc() {
    let fseq_message = FSeqMessage {
        address_prefix: "".to_string(),
        sequence: 595,
    };

    let converted = fseq_message.to_osc();

    let expected = OscMessage {
        addr: "/tuio/2Dcur".to_string(),
        args: vec![OscType::String("fseq".to_string()), OscType::Int(595)],
    };

    assert_eq!(converted, expected);

    let converted_back = FSeqMessage::from_osc("", &converted).unwrap();

    assert_eq!(converted_back, fseq_message);
}

#[test]
fn tuio_bundle_to_osc() {
    let tuio_bundle = TuioBundle {
        source_message: Some(SourceMessage {
            address_prefix: "".to_string(),
            source: "test@127.0.0.1".to_string(),
        }),
        alive_message: AliveMessage {
            address_prefix: "".to_string(),
            session_ids: vec![99, 88, 2],
        },
        set_messages: vec![
            SetMessage {
                address_prefix: "".to_string(),
                session_id: 99,
                x: 0.,
                y: 0.,
                x_velocity: 0.,
                y_velocity: 0.,
                motion_acceleration: 0.,
            },
            SetMessage {
                address_prefix: "".to_string(),
                session_id: 88,
                x: 0.2,
                y: 1.,
                x_velocity: 9.,
                y_velocity: -0.5,
                motion_acceleration: -100.,
            },
            SetMessage {
                address_prefix: "".to_string(),
                session_id: 2,
                x: 0.5,
                y: 0.5,
                x_velocity: 20.,
                y_velocity: -0.9,
                motion_acceleration: 0.,
            },
        ],
        fseq_message: FSeqMessage {
            address_prefix: "".to_string(),
            sequence: 9001,
        },
    };

    let converted = tuio_bundle.to_osc();

    let expected = OscBundle {
        timetag: OscTime {
            seconds: 0,
            fractional: 1,
        },
        content: vec![
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![
                    OscType::String("source".to_string()),
                    OscType::String("test@127.0.0.1".to_string()),
                ],
            },
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![
                    OscType::String("alive".to_string()),
                    OscType::Int(99),
                    OscType::Int(88),
                    OscType::Int(2),
                ],
            },
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![
                    OscType::String("set".to_string()),
                    OscType::Int(99),
                    OscType::Float(0.),
                    OscType::Float(0.),
                    OscType::Float(0.),
                    OscType::Float(0.),
                    OscType::Float(0.),
                ],
            },
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![
                    OscType::String("set".to_string()),
                    OscType::Int(88),
                    OscType::Float(0.2),
                    OscType::Float(1.),
                    OscType::Float(9.),
                    OscType::Float(-0.5),
                    OscType::Float(-100.),
                ],
            },
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![
                    OscType::String("set".to_string()),
                    OscType::Int(2),
                    OscType::Float(0.5),
                    OscType::Float(0.5),
                    OscType::Float(20.),
                    OscType::Float(-0.9),
                    OscType::Float(0.),
                ],
            },
            OscMessage {
                addr: "/tuio/2Dcur".to_string(),
                args: vec![OscType::String("fseq".to_string()), OscType::Int(9001)],
            },
        ]
        .drain(..)
        .map(OscPacket::Message)
        .collect(),
    };

    assert_eq!(converted, expected);

    let converted_back = TuioBundle::from_osc("", &converted).unwrap();

    assert_eq!(converted_back, tuio_bundle);
}
