use std::net::SocketAddr;

#[derive(Debug)]
pub struct SourceIdentifier {
    base_name: String,
    append_ip_address: bool,
}

impl SourceIdentifier {
    pub fn new(base_name: String) -> Result<Self, String> {
        let matches = base_name.match_indices('@');

        match matches.count() {
            0 => Ok(Self {
                base_name,
                append_ip_address: false,
            }),
            1 => {
                if base_name.ends_with('@') {
                    Ok(Self {
                        base_name: base_name[..base_name.len()].to_string(),
                        append_ip_address: true,
                    })
                } else if base_name.starts_with('@') {
                    Err(format!("'{}' may not start with an '@'.", base_name))
                } else {
                    Ok(Self {
                        base_name,
                        append_ip_address: false,
                    })
                }
            }
            _ => Err(format!("'{}' contains multiple '@' signs.", base_name)),
        }
    }

    pub fn format(&self, ip_address: &SocketAddr) -> String {
        match self.append_ip_address {
            true => format!("{}@{}", self.base_name, ip_address),
            false => self.base_name.clone(),
        }
    }
}
