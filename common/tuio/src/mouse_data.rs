use std::{collections::HashMap, iter::FromIterator, time::SystemTime};

#[derive(Debug)]
pub struct MouseData {
    pub timestamp: SystemTime,
    pub cursors: HashMap<u16, CursorData>,
}

#[derive(Debug, Copy, Clone)]
pub struct CursorData {
    pub x: f32,
    pub y: f32,
}

#[derive(Debug)]
pub struct ComputedMouseData {
    pub timestamp: SystemTime,
    pub cursors: HashMap<u16, ComputedCursorData>,
}

#[derive(Debug)]
pub struct ComputedCursorData {
    pub cursor_data: CursorData,
    pub computed_part_of_cursor_data: Option<ComputedPartOfCursorData>,
}

#[derive(Debug)]
pub struct ComputedPartOfCursorData {
    pub x_velocity: f32,
    pub y_velocity: f32,
    pub general_velocity: f32,
    pub motion_acceleration: Option<f32>,
}

impl ComputedMouseData {
    pub fn new(
        mouse_data: &MouseData,
        previous_computed_mouse_data: &Option<ComputedMouseData>,
    ) -> Self {
        Self {
            timestamp: mouse_data.timestamp,
            cursors: HashMap::from_iter(mouse_data.cursors.iter().map(
                |(cursor_id, cursor_data)| {
                    (
                        *cursor_id,
                        ComputedCursorData {
                            cursor_data: *cursor_data,
                            computed_part_of_cursor_data: previous_computed_mouse_data
                                .as_ref()
                                .map(|previous_computed_mouse_data| {
                                    previous_computed_mouse_data.cursors.get(cursor_id).map(
                                        |computed_cursor_data| {
                                            (
                                                previous_computed_mouse_data.timestamp,
                                                computed_cursor_data,
                                            )
                                        },
                                    )
                                })
                                .flatten()
                                .and_then(|(previous_timestamp, previous_computed_cursor)| {
                                    let time_passed = match mouse_data
                                        .timestamp
                                        .duration_since(previous_timestamp)
                                    {
                                        Err(_) => return None,
                                        Ok(result) => result,
                                    };
                                    let time_passed_in_seconds = time_passed.as_secs_f32();

                                    let delta_x =
                                        cursor_data.x - previous_computed_cursor.cursor_data.x;
                                    let delta_y =
                                        cursor_data.y - previous_computed_cursor.cursor_data.y;

                                    let x_velocity = delta_x / time_passed_in_seconds;
                                    let y_velocity = delta_y / time_passed_in_seconds;

                                    let general_velocity = (delta_x.powi(2) + delta_y.powi(2))
                                        .sqrt()
                                        / time_passed_in_seconds;

                                    let mut motion_acceleration = None;

                                    if let Some(previous_computed_part_of_cursor_data) =
                                        &previous_computed_cursor.computed_part_of_cursor_data
                                    {
                                        motion_acceleration = Some(
                                            (general_velocity
                                                - previous_computed_part_of_cursor_data
                                                    .general_velocity)
                                                / time_passed_in_seconds,
                                        );
                                    }

                                    Some(ComputedPartOfCursorData {
                                        x_velocity,
                                        y_velocity,
                                        general_velocity,
                                        motion_acceleration,
                                    })
                                }),
                        },
                    )
                },
            )),
        }
    }

    pub fn to_mouse_data(&self) -> MouseData {
        MouseData {
            timestamp: self.timestamp,
            cursors: HashMap::from_iter(self.cursors.iter().map(
                |(cursor_id, computed_cursor_data)| (*cursor_id, computed_cursor_data.cursor_data),
            )),
        }
    }
}
