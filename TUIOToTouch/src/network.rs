use std::iter::FromIterator;
use std::sync::mpsc::Sender;
use std::sync::RwLockReadGuard;
use std::time::{Duration, Instant};
use std::{collections::HashMap, net::UdpSocket};

use constants::DEFAULT_LISTENER_REGISTERING_OSC_ADDRESS;
use rosc::{decoder, OscMessage, OscPacket};
use tuio::messages::TuioBundle;
use tuio::mouse_data::CursorData;

use crate::arguments::{get_arguments, Arguments};

pub fn receive_tuio(sender: Sender<HashMap<u16, CursorData>>) -> Result<(), String> {
    let arguments = get_arguments().map_err(|_| "Could not load the arguments.")?;

    let socket = UdpSocket::bind((arguments.listening_address.to_owned(), arguments.port))
        .map_err(|error| error.to_string())?;

    if let Some(register_socket) = &arguments.register_socket {
        let osc_message = OscMessage {
            addr: DEFAULT_LISTENER_REGISTERING_OSC_ADDRESS.to_owned(),
            args: vec![],
        };

        let raw_packet =
            rosc::encoder::encode(&OscPacket::Message(osc_message)).map_err(|error| {
                format!(
                    "Cannot encode the registering OSC message.\nError: {:#?}",
                    error
                )
            })?;

        socket
            .send_to(&raw_packet, register_socket)
            .map_err(|error| {
                format!(
                    "Cannot register as listener for socket '{}'.\nError: {:#?}",
                    register_socket, error
                )
            })?;
    }

    // Theoretical limitation to UDP content length
    // Does not support IPv6 jumbograms.
    let mut raw_buffer = [0; 65527];

    let mut latest_fseq_plus_instant: Option<(u16, Instant)> = None;
    let mut read_timeout_set = false;

    fn reset_due_to_timeout(
        sender: &Sender<HashMap<u16, CursorData>>,
        latest_fseq_plus_instant: &mut Option<(u16, Instant)>,
    ) -> Result<(), String> {
        *latest_fseq_plus_instant = None;
        sender.send(HashMap::new()).map_err(|e| e.to_string())?;

        Ok(())
    }

    /// Returns true if the expected fseq was reset
    fn check_timeout(
        arguments: &RwLockReadGuard<Arguments>,
        latest_fseq_plus_instant: &mut Option<(u16, Instant)>,
    ) -> bool {
        if let Some((_, instant)) = latest_fseq_plus_instant {
            if instant.elapsed().as_millis() >= arguments.fseq_timeout_in_ms as u128 {
                *latest_fseq_plus_instant = None;

                true
            } else {
                false
            }
        } else {
            false
        }
    }

    /// Returns true if it was reset and therefore mouse data sent
    fn check_timeout_and_possibly_reset(
        arguments: &RwLockReadGuard<Arguments>,
        sender: &Sender<HashMap<u16, CursorData>>,
        latest_fseq_plus_instance: &mut Option<(u16, Instant)>,
    ) -> Result<bool, String> {
        if check_timeout(arguments, latest_fseq_plus_instance) {
            reset_due_to_timeout(sender, latest_fseq_plus_instance)?;

            Ok(true)
        } else {
            Ok(false)
        }
    }

    loop {
        match latest_fseq_plus_instant {
            None => {
                if read_timeout_set {
                    socket.set_read_timeout(None).map_err(|e| e.to_string())?;
                    read_timeout_set = false;
                }
            }
            Some(_) => {
                if !read_timeout_set {
                    socket
                        .set_read_timeout(Some(Duration::from_millis(
                            arguments.fseq_timeout_in_ms.into(),
                        )))
                        .map_err(|e| e.to_string())?;
                    read_timeout_set = true;
                }
            }
        }

        let number_of_bytes = match socket.recv(&mut raw_buffer) {
            Ok(number_of_bytes) => number_of_bytes,
            Err(error) => {
                if let Some(error_code) = error.raw_os_error() {
                    // No new package received within the timeout
                    if error_code == 11 {
                        reset_due_to_timeout(&sender, &mut latest_fseq_plus_instant)?;
                        continue;
                    }

                    return Err(error.to_string());
                }

                return Err(error.to_string());
            }
        };

        let raw_packet = &raw_buffer[..number_of_bytes];

        let osc_bundle = if let OscPacket::Bundle(osc_bundle) = match decoder::decode(raw_packet) {
            Ok(osc_packet) => osc_packet,
            Err(_) => {
                check_timeout_and_possibly_reset(
                    &arguments,
                    &sender,
                    &mut latest_fseq_plus_instant,
                )?;
                continue;
            }
        } {
            osc_bundle
        } else {
            check_timeout_and_possibly_reset(&arguments, &sender, &mut latest_fseq_plus_instant)?;
            continue;
        };

        let tuio_data = match TuioBundle::from_osc(&arguments.address_prefix, &osc_bundle) {
            Ok(tuio_data) => tuio_data,
            Err(_) => {
                check_timeout_and_possibly_reset(
                    &arguments,
                    &sender,
                    &mut latest_fseq_plus_instant,
                )?;
                continue;
            }
        };

        {
            let fseq = tuio_data.fseq_message.sequence;

            let fseq_reset = check_timeout(&arguments, &mut latest_fseq_plus_instant);

            match fseq {
                0.. => {
                    let fseq = fseq as u16;

                    match latest_fseq_plus_instant {
                        None => latest_fseq_plus_instant = Some((fseq, Instant::now())),
                        Some((ref mut last_fseq, ref mut instant)) => {
                            if fseq > *last_fseq {
                                *last_fseq = fseq;
                                *instant = Instant::now();
                            } else {
                                continue;
                            }
                        }
                    }
                }
                -1 => {
                    if let Some((_, ref mut instant)) = latest_fseq_plus_instant {
                        *instant = Instant::now();
                    }

                    if fseq_reset {
                        reset_due_to_timeout(&sender, &mut latest_fseq_plus_instant)?;
                    }

                    continue;
                }
                i32::MIN..=-2 => {
                    println!("Received a TUIO bundle with a fseq of {}", fseq);

                    if fseq_reset {
                        reset_due_to_timeout(&sender, &mut latest_fseq_plus_instant)?;
                    }

                    continue;
                }
            }
        }

        let cursor_data: HashMap<u16, CursorData> =
            HashMap::from_iter(tuio_data.set_messages.iter().map(|set_message| {
                (
                    set_message.session_id,
                    CursorData {
                        x: set_message.x,
                        y: set_message.y,
                    },
                )
            }));

        sender
            .send(cursor_data)
            .map_err(|error| error.to_string())?;
    }
}
