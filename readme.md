# Overview and Goal
The goal of this project is to allow separating the computer where touch device input is received from the computer where this data is being facilitated. This is achieved by reading the touch data on one computer, converting it, and sending it over the network via the [TUIO](https://www.tuio.org/) protocol and having a second computer that receives the TUIO data via e.g. LAN and acting upon it, for example by emulating a device that behaves just like the original data source.

## Context
This project was created by me, Bjarne Kühl, as part of a module at the Fachhochschule Kiel - University of Applied Sciences. Thanks to my instructor Prof. Dr. Steffen Prochnow for supporting the research concerning OSC and TUIO as well as interacting with and emulating input devices under Linux.

## Contents
This project provides two programs:

- **TouchToTUIO**
- **TUIOToTouch**

## TL;DR
### Touch data transmitter: TouchToTUIO
Install Rust, Cargo, Gem, and fpm (assumes you are using Bash and a Debian derivative):
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain stable --profile complete
echo 'export PATH="$PATH:$HOME/.cargo/bin"' >> ~/.bashrc
source ~/.bashrc

sudo apt-get update
sudo apt-get install -y ruby
sudo gem install fpm
```

Clone the repository:
```bash
git clone --recurse-submodules https://gitlab.com/Nixname/TouchAndTUIO.git
cd TouchAndTUIO
# In case you did not recurse the submodules beforehand
git submodule update --init --recursive
```

Compile, package and install TouchToTUIO.

Using 64 bit Intel processor:
```bash
cd TouchToTUIO
cargo build --target x86_64-unknown-linux-gnu --release
./package.py deb
sudo dpkg -i touch_to_tuio-0.1.1-1-x86_64.deb
```

On Raspberry PI:
```bash
cd TouchToTUIO
cargo build --target armv7-unknown-linux-gnueabihf --release
./package.py deb
sudo dpkg -i touch_to_tuio-0.1.1-1-armhf.deb
```

### Touch data receiver: TUIOToTouch
Install Rust, Cargo, Gem, fpm, and libudev-dev (assumes you are using Bash and a Debian derivative):
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain stable --profile complete
echo 'export PATH="$PATH:$HOME/.cargo/bin"' >> ~/.bashrc
source ~/.bashrc

sudo apt-get update
sudo apt-get install -y ruby
sudo gem install fpm

sudo apt-get install -y libudev-dev
```

Clone the repository:
```bash
git clone --recurse-submodules https://gitlab.com/Nixname/TouchAndTUIO.git
cd TouchAndTUIO
# In case you did not recurse the submodules beforehand
git submodule update --init --recursive
```

Compile, package and install TUIOToTouch.

Using 64 bit Intel processor:
```bash
cd TUIOToTouch
cargo build --target x86_64-unknown-linux-gnu --release
./package.py deb
sudo dpkg -i tuio_to_touch-0.1.1-1-x86_64.deb
```

On Raspberry PI:
```bash
cd TUIOToTouch
cargo build --target armv7-unknown-linux-gnueabihf --release
./package.py deb
sudo dpkg -i tuio_to_touch-0.1.1-1-armhf.deb
```

### Run locally for testing
```bash
sudo touch_to_tuio --block-event
sudo tuio_to_touch
# Or to behave like a touchpad as opposed to a touchscreen
sudo tuio_to_touch --touchpad-mode
```

### Run as background task (preferably service) forwarding data via this [OSC broadcaster](https://gitlab.com/Nixname/OSCBroadcaster.git)
#### OSC broadcaster (IP: 192.168.0.3)
```bash
osc_broadcaster
```

#### Touch data transmitter: TouchToTUIO (IP: 192.168.0.5)
Adjust `--device-name` to your requirements. Hint: You can get a list of all available devices using the flag `--list-devices`.
```bash
sudo touch_to_tuio --non-interactive --block-event --watch --device-name 'Wacom Bamboo 16FG 4x5 Finger' --destination 192.168.0.3 --port 9000 --prefix /TouchAndTUIO
```

#### Touch data receiver: TUIOToTouch (IP: 192.168.0.8)
```bash
sudo tuio_to_touch --register-socket 192.168.0.3:9000 --port 9001 --prefix /TouchAndTUIO
# Or to behave like a touchpad as opposed to a touchscreen
sudo tuio_to_touch --register-socket 192.168.0.3:9000 --port 9001 --prefix /TouchAndTUIO --touchpad-mode
```

# TouchToTUIO
## The aim
This program's aim is to read absolute cursor data from touch screens, touch pads, touch enabled beamers, and the like, convert it to [TUIO](https://www.tuio.org/), and provide it via network to consumers.

![10 Finger Demonstation (TUIO viewer in use: <https://github.com/mkalten/TUIO11_CPP>)](./readme-src/10FingerDemo.png)

The above image shows ten finger positions transmitted via TUIO and a trail of their past locations. In use was a graphics tablet connected to a headless Raspberry Pi running this program collecting, transforming, and transmitting the tablet's data. The data was received on a local computer running this demonstration program (see <https://github.com/mkalten/TUIO11_CPP>).

## Compatibility
At this point in time the application can only run on Linux. This is not due to Rust being a compiled language (in fact, it can be compiled to all the main OSes, I can think of: Windows, macOS, Linux, and FreeBSD) but instead due to the way the mouse data is collected. The mouse data cannot be collected in the usual way from a graphics library such as GTK or QT, since this would firstly require an X11 server or similar and you might want to run this program on a headless system like a Raspberry Pi and secondly it would most likely only provide data about a single cursor (the mouse cursor) which completely neglects the requirement for multi touch.

Hence the data has to be collected before any graphics system is involved and there are no cross platform libraries for this use case around, as far as I am aware (some looked promising but turned out to depend on graphical systems after all or had the limitation of providing only a single cursor). Therefore the solution had to be created from the ground up, reading raw data from `/dev/input/event*`, thus resulting in a platform specific solution.

You are welcome to improve upon this and provide support for other platforms as well.

## How to compile this program
Ensure Cargo and Rust are installed. You can install them via this command:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain stable --profile complete
```
Or for more customisability:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Make sure `rustc` and `cargo` are available from your shell. The directory `$HOME/.cargo/bin` needs to exist in your `$PATH`

`cd` from this project's root directory into `TouchToTUIO` and execute:
```bash
cargo build
```
Or to make a release build:
```bash
cargo build --release
```

The binary is available as `./target/debug/touch_to_tuio` or `./target/release/touch_to_tuio` respectively.

As a convenience command you can execute it with
```bash
cargo run -- --help
```
or
```bash
cargo run --release -- --help
```
respectively.

### Compiling from the Raspberry Pi
See the general installation step further up using `curl` and `sh`.

Installing Cargo and Rust via the following command is not viable:
```bash
sudo apt-get install cargo
```
That is because the version (cargo: 0.43.1-3~deb10u1+rpi1) currently (date of writing: 23.10.2021) available in the standard repos for the Raspberry does not function. When calling `cargo fetch` on this project, a segmentation fault occurs.

### Cross compilation for the Raspberry Pi
#### Variant 1: System wide cross compiler installation
```bash
sudo apt-get install gcc-arm-linux-gnueabihf
```

Download the standard libraries:
```bash
rustup target add armv7-unknown-linux-gnueabihf
```

Execute the build:
```bash
cargo build --target armv7-unknown-linux-gnueabihf
```

Execute build for release (s.a.)
```bash
cargo build --target armv7-unknown-linux-gnueabihf --release
```

#### Variant 2: Using [Cross](https://github.com/rust-embedded/cross)
*Warning: Cross is not a viable alternative anymore as it does not support path dependencies to local crates, but this exact feature is being used by this project.*

Make sure Cross is installed:
```bash
cargo install cross
```

Cross relies on Docker or Podman for hosting a build environment, so make sure either of them is available in your `$PATH` and does not need `sudo` prepended to function.

Then simply replace `cargo` by `cross` in your commands and do not forget to specify the target.
```bash
cross build --target armv7-unknown-linux-gnueabihf
```

## Packaging
### Requirement
The packaging process facilitates the program `fpm`. You can install it by executing
```bash
sudo gem install fpm
```
assuming you have Ruby and its package manager `gem` installed.

### Process
Execute the build for which you want to package the program specifying the target triple explicitly e.g. `cargo build --target x86_64-unknown-linux-gnu --release`. Execute the script `package.py` directly afterwards. If you want it to be packaged as a `.deb` file execute `./package.py deb` or if it shall be packaged for Arch based distributions execute `./package.py pacman`.

Performing the build writes some necessary meta information such as the package version and the target architecture into the ephemeral file `.variables`. As this file is being overwritten by each build even those performed implicitly by your IDE, you should run the packaging script rather sooner than later.

To find out your target triple:
```bash
rustup target list
```

To create the release binary on Intel 64bit architecture:
```bash
cargo build --target x86_64-unknown-linux-gnu  --release
```

To create the release binary on Raspberry PI Arm architecture:
```bash
cargo build --target armv7-unknown-linux-gnueabihf --release
```

# TUIOToTouch
Now that there is a way to convert absolute mouse data (multi-touch) to TUIO, there is the need to do the reverse and control a PC's cursor (preferably multi-touch again).

A general cross-platform solution for this exists: <https://github.com/mkalten/TUIO11_Mouse>

The disadvantage is, that it only supports setting the mouse cursor and therefore again ignores the multi-touch requirement.

## Windows
For Windows there already exists a program also fulfilling the multi-touch requirement. Hurray! <https://github.com/michaelosthege/TouchInjector>

## Linux
For Linux I did not found such a program, so I created it myself. The result can be found in the directory `TUIOToTouch`. The instructions on compilation and packaging for TouchToTUIO also hold true for TUIOToTouch. For compilation you need to install the package `libudev-dev`:
```bash
sudo apt-get install libudev-dev
```

The reason, for why this solution is not cross platform, is essentially the same as for why TouchToTUIO is not. It is due to the way in which the touch device is being emulated. The kernel module `uinput` is being used making the solution platform dependent. Platform agnostic solutions would again ignore the requirement for multi-touch.

### Multi-Touch
This solution supports multi-touch to your heart's content. Now you may ask yourself, "So why is it, that I cannot scroll using two fingers?" This is not due to the program having a bug, but instead that the Linux distributions, I tried it on, simply do not provide proper support for multi-touch touchscreens. So this is not something this program can magically solve.

I've come to this conclusion by simply connecting a multi-touch touchscreen (a beamer that provides multi-touch input) and trying to scroll in Firefox without using any of these programs, so no TUIO or device emulation involved. This just selected text, so this cannot be a problem with this program.

Another clue is, that this program can be put into touchpad mode (using the CLI switch `--touchpad-mode`) making it behave like a touchpad as opposed to a touchscreen and while in this mode, scrolling with two fingers works just fine. And technically the only difference between these two modes is setting the device property `INPUT_PROP_POINTER` (touchpad mode) as opposed to `INPUT_PROP_DIRECT` (touchscreen mode). See <https://www.kernel.org/doc/html/v4.15/input/event-codes.html#input-prop-direct-input-prop-pointer> for reference.

## macOS
It seems to be possible to create a multi-touch app for macOS as well, but it involves jumping through quite a number of hoops. Firstly you need to create an app bundle and develop a system extension contained therein most likely using XCode. Secondly you will almost certainly need to receive a developer's license and signing key from Apple and you need to make sure "The entitlements in the code signature match the entitlements granted to your development team.", which sounds quite intimidating.

Useful resources:

- <https://developer.apple.com/documentation/systemextensions/installing_system_extensions_and_drivers>
- <https://developer.apple.com/documentation/hiddriverkit/iohideventservice/3395539-dispatchdigitizertouchevent>
- <https://stackoverflow.com/a/33703765> Although this doesn't involve as many hoops, it still presents the restriction of being able to only set a single mouse cursor, throwing the multi-touch requirement out the window. So this solution would have no advantage over the general Java-based solution.

# Features
- Robust software
- Provides helpful error messages most of the time.
- Proper documentation (this document)
  - `--help` with a little documentation on each parameter as well as the defaults for each parameter
- Packaging for both Debian and derivatives as well as Arch and derivatives
- Can be configured via the CLI
  - No need to recompile for most changes

## TouchToTUIO:
Useful parameters:

- `-w`, `--watch`: Prevents the programs from exiting, when the device cannot be found, allowing plug and play behaviour
- `-n`, `--device-name`: To automatically select the device based on its name instead of an arbitrary number (e.g. `Wacom Bamboo 16FG 4x5 Finger` instead of `/dev/input/event28`)
- `-b`, `--block-event`: To prevent the device from affecting the mouse cursor on the transmitting machine
- `--non-interactive`

Robust background service:

- Touch device disconnects: Program does not crash
- Touch device connects: Program catches the device's output again and carries on transmitting
- Network is removed: Program does not crash
- Network is connected: Program resumes transmitting data
- Computer crashes because of power loss: Program crashes
- Computer powers on again: Program can be restarted via a service and commence operation

The program also works, if at startup there is initially no network or no device to get the touch data from.

- Transmits keep alive packets
  - Every 5 seconds: Adjustable
- No dependency on any graphical environment
  - Can run even on a headless Raspberry PI
  - No need to change a thing when upgrading from X11 to Wayland
- Can use a prefix for the OSC Address Pattern: Adjustable
- Supports multi-touch
- Can output a list of all available devices
- Three options to select a device
  - Interactive selection after having seen a list of all available devices
  - Selection by event path (e.g. /dev/input/event8)
  - Selection by device name (e.g. LCT)
- Can gain exclusive access to the device resulting in no other readers having access
  - Particularly useful if used on a graphical system and the device should not move the mouse there
- Supports mirroring along x and y axes and rotations in 90 degree steps

## TUIOToTouch
Useful parameters:

- `--touchpad-mode`: Make the emulated device behave like a touchpad as opposed to the default behaviour of a touchscreen.
- `-R`, `--register-socket`: Register this computer as a listener for the OSC broadcaster denoted by the given socket. Built for interoperability with this specific [OSC broadcaster](https://gitlab.com/Nixname/OSCBroadcaster.git).

Highlights:

- Uses timeouts: Automatically releases the current touch if no new packages were received for a certain amount of time (10 seconds: adjustable)
- Can use a prefix for the OSC Address Pattern: Adjustable
- Suports multi-touch
  - Touchscreen mode: Supports multi-touch
    - Linux (specifically Xorg or some other system level software) does not support multi-touch for touchscreen input
  - Touchpad mode: Supports multi-touch
    - Linux supports multi-touch for touchpad input since this is way more mainstream than multi-touch from touchscreens
- Optionally registers itself as a listener on [this OSC broadcaster](https://gitlab.com/Nixname/OSCBroadcaster.git)
