use std::sync::{RwLock, RwLockReadGuard};

use clap::Parser;

use constants::{
    ADDRESS_PATTERN_PREFIX_DEFAULT, DEFAULT_UINPUT_PATH, DEVICE_DEFAULT_BUS_STR,
    DEVICE_DEFAULT_PRODUCT_STR, DEVICE_DEFAULT_VENDOR_STR, DEVICE_DEFAULT_VERSION_STR,
    DEVICE_DELAY_IN_MS_AFTER_CREATION_STR, RECEIVER_FSEQ_TIMEOUT_IN_MS_STR,
    RECEIVING_DEFAULT_DESTINATION_ADDRESS, RECEIVING_DEFAULT_DESTINATION_PORT_STR,
    SOURCE_IDENTIFIER_BASE,
};

/// Program to receive TUIO data and emulate a touch device
#[derive(Debug, Parser)]
pub struct Arguments {
    /// The port to be listening on
    #[clap(short, long, default_value = RECEIVING_DEFAULT_DESTINATION_PORT_STR)]
    pub port: u16,
    /// The address to be listening on
    #[clap(short, long = "listen", default_value = RECEIVING_DEFAULT_DESTINATION_ADDRESS)]
    pub listening_address: String,
    /// The OSC address pattern prefix to listen for
    #[clap(short = 'P', long = "prefix", default_value = ADDRESS_PATTERN_PREFIX_DEFAULT)]
    pub address_prefix: String,

    /// The socket to which this program should register itself as a listener. Should be of the form <ip>:<port> e.g. 127.0.0.1:9000
    #[clap(short = 'R', long)]
    pub register_socket: Option<String>,

    /// Behave like a touchpad as opposed to behaving like a touchscreen
    #[clap(long)]
    pub touchpad_mode: bool,

    /// The number of milliseconds to wait after device creation before sending the
    /// first bit of data. Set to zero to disable the delay.
    #[clap(long = "delay", default_value = DEVICE_DELAY_IN_MS_AFTER_CREATION_STR)]
    pub delay_ms_after_creation: u16,

    /// The name to set up for the device
    #[clap(long, default_value = SOURCE_IDENTIFIER_BASE)]
    pub device_name: String,
    /// The bus to set up for the device
    #[clap(long, default_value = DEVICE_DEFAULT_BUS_STR)]
    pub device_bus: u16,
    /// The vendor to set up for the device
    #[clap(long, default_value = DEVICE_DEFAULT_VENDOR_STR)]
    pub device_vendor: u16,
    /// The product to set up for the device
    #[clap(long, default_value = DEVICE_DEFAULT_PRODUCT_STR)]
    pub device_product: u16,
    /// The version to set up for the device
    #[clap(long, default_value = DEVICE_DEFAULT_VERSION_STR)]
    pub device_version: u16,

    /// Timeout in milliseconds after which a connection is considered to be lost
    #[clap(long = "fseq-timeout", default_value = RECEIVER_FSEQ_TIMEOUT_IN_MS_STR)]
    pub fseq_timeout_in_ms: u16,

    /// The path to use for interacting with the uinput kernel module
    #[clap(long, default_value = DEFAULT_UINPUT_PATH)]
    pub uinput_path: String,
}

lazy_static::lazy_static! {
    static ref ARGUMENTS: RwLock<Arguments> = RwLock::new(Arguments {
        listening_address: "".to_owned(),
        port: 0,
        address_prefix: "".to_owned(),
        touchpad_mode: false,
        delay_ms_after_creation: 0,
        device_name: "".to_owned(),
        device_bus: 0,
        device_vendor: 0,
        device_product: 0,
        device_version: 0,
        fseq_timeout_in_ms: 0,
        uinput_path: "".to_owned(),
        register_socket: None,
    });

    static ref ARGUMENTS_INITIALISED: RwLock<bool> = RwLock::new(false);
}

pub fn initialise_arguments(input: Arguments) -> Result<(), String> {
    let mut arguments = ARGUMENTS
        .write()
        .map_err(|_| "The arguments are poisoned.".to_string())?;

    let mut arguments_initialised = ARGUMENTS_INITIALISED
        .write()
        .map_err(|_| "arguments_initialised is poisoned.".to_string())?;

    match *arguments_initialised {
        true => {
            return Err("The arguments were already set.".to_string());
        }
        false => {
            *arguments_initialised = true;
        }
    }

    *arguments = input;

    Ok(())
}

pub fn get_arguments<'a>() -> Result<RwLockReadGuard<'a, Arguments>, ()> {
    ARGUMENTS.read().map_err(|_| ())
}
