use clap::Parser;

use constants::{
    ADDRESS_PATTERN_PREFIX_DEFAULT, SENDER_KEEP_ALIVE_DURATION_IN_MS_STR,
    SENDING_DEFAULT_DESTINATION_ADDRESS, SENDING_DEFAULT_DESTINATION_PORT_STR,
    SOURCE_IDENTIFIER_BASE,
};

/// Capture absolute mouse data and transmit via TUIO
#[derive(Debug, Parser)]
pub struct Arguments {
    /// The device's event path e.g. /dev/input/event5
    #[clap(short, long = "event")]
    pub event_path: Option<String>,
    /// The device's name, which will be used to get the right event path
    #[clap(short = 'n', long)]
    pub device_name: Option<String>,
    /// The source IP address to send data from
    #[clap(short = 's', long = "source", default_value = "0.0.0.0")]
    pub source_address: String,
    /// The source port to send data from
    #[clap(short = 'S', long, default_value = "0")]
    pub source_port: u16,
    /// The destination IP address to send data to
    #[clap(short = 'd', long = "destination", default_value = SENDING_DEFAULT_DESTINATION_ADDRESS)]
    pub destination_address: String,
    /// The destination port to send data to
    #[clap(short = 'p', long = "port", default_value = SENDING_DEFAULT_DESTINATION_PORT_STR)]
    pub destination_port: u16,
    /// The source identifier to use in the TUIO messages. Set to an empty string to disable the
    /// source message
    #[clap(short = 'i', long = "identifier", default_value = SOURCE_IDENTIFIER_BASE)]
    pub source_identifier: String,
    /// The OSC address pattern prefix to use
    #[clap(short = 'P', long = "prefix", default_value = ADDRESS_PATTERN_PREFIX_DEFAULT)]
    pub addres_prefix: String,
    /// Whether the device's events should be blocked to other readers
    #[clap(short = 'b', long)]
    pub block_event: bool,
    /// Exit after listing all available event devices
    #[clap(short = 'l', long)]
    pub list_devices: bool,
    /// Execute without requiring any user input other than the given arguments
    #[clap(long)]
    pub non_interactive: bool,
    /// Mirror the x coordinates of the received data
    #[clap(long)]
    pub mirror_x: bool,
    /// Mirror the y coordinates of the received data
    #[clap(long)]
    pub mirror_y: bool,
    /// Rotate by 90° clock-wise. Can be stacked. Rotations are applied after mirroring.
    #[clap(short = 'r', long = "rotate", parse(from_occurrences))]
    pub number_of_rotations: u8,
    /// Allow the device to be connected and disconnected without this program emitting an error
    #[clap(short, long)]
    pub watch: bool,
    /// The number of milliseconds of inactivity after which a keep alive message shall be transmitted. Set to 0 to disable keep alive messages.
    #[clap(long = "keep_alive", default_value = SENDER_KEEP_ALIVE_DURATION_IN_MS_STR)]
    pub keep_alive_interval_in_ms: u16,
}
