#!/usr/bin/env invoke

from invoke import task

import os
import subprocess

from pathlib import Path

topLevelDirectory = os.path.dirname(os.path.realpath(__file__))

os.chdir(topLevelDirectory)


projects = [
    "TouchToTUIO",
    "TUIOToTouch",
    "common/constants",
    "common/helpers",
    "common/tuio",
]
projectsAbsolutePaths = map(
    lambda x: os.path.join(topLevelDirectory, x),
    projects,
)

allProjects = [
    *projects,
    "common/multi-map",
    "common/uinput-tokio",
]
allProjectsAbsolutePaths = map(
    lambda x: os.path.join(topLevelDirectory, x),
    projects,
)


def checkExistence(absoluteProjectPath):
    if not Path(absoluteProjectPath).is_dir():
        raise Exception(f"The project '{absoluteProjectPath}' does not exist.")


@task
def format(c):
    for absProjectPath in projectsAbsolutePaths:
        checkExistence(absProjectPath)

        print(f"Inside: {absProjectPath}")

        subprocess.run(
            [
                "cargo",
                "fmt",
            ],
            cwd=absProjectPath,
        )


@task
def build(c):
    for absProjectPath in allProjectsAbsolutePaths:
        checkExistence(absProjectPath)

        print(f"Inside: {absProjectPath}")

        subprocess.run(
            [
                "cargo",
                "build",
            ],
            cwd=absProjectPath,
        )


@task
def clippy(c):
    for absProjectPath in allProjectsAbsolutePaths:
        checkExistence(absProjectPath)

        print(f"Inside: {absProjectPath}")

        subprocess.run(
            [
                "cargo",
                "clippy",
            ],
            cwd=absProjectPath,
        )


@task
def formatPython(c):
    subprocess.run(
        [
            "black",
            __file__,
        ],
    )
