use std::{
    collections::BTreeMap,
    io::{self, Write},
    path::PathBuf,
};

use text_io::try_read;

use crate::{mouse_reader::get_event_name_mapping, variables::get_constants};

pub fn display_devices(event_name_mapping: &BTreeMap<u16, (PathBuf, String, String)>) {
    println!("Available devices:");

    for (key, value) in event_name_mapping {
        println!("{}: {}: {}", key, value.0.display(), value.2);
    }
}

pub fn get_event_path_by_device_name(given_device_name: &str) -> Result<Option<PathBuf>, String> {
    let mapping = get_event_name_mapping()?;

    let matches: Vec<_> = mapping
        .iter()
        .filter(|(_key, (_event_path, _event_name, device_name))| device_name == given_device_name)
        .collect();

    match matches.len() {
        0 => Ok(None),
        1 => Ok(Some(matches[0].1 .0.clone())),
        _ => Err(
            "Multiple devices with equal names were found. Specify the event path instead."
                .to_string(),
        ),
    }
}

pub fn get_event_path(
    entered_device_name: &mut Option<String>,
) -> Result<Result<PathBuf, String>, String> {
    let constants = get_constants().map_err(|_| "Could not access the constants.".to_string())?;

    match constants.device_name.as_ref().or_else(|| entered_device_name.as_ref()) {
        Some(given_device_name) => {
            match get_event_path_by_device_name(given_device_name)? {
                Some(event_path) => Ok(Ok(event_path)),
                None => Ok(Err(format!("No event path could be found for the device name '{}'", given_device_name))),
            }
        }
        None => match &constants.event_path {
            Some(event_path) => Ok(Ok(event_path.into())),
            None => match constants.non_interactive {
                true => Err("Neither a device name nor an event path nor the option to list all devices was specified.".to_string()),
                false => {
                    let mapping = get_event_name_mapping()?;

                    display_devices(&mapping);

                    if mapping.is_empty() {
                        Err("No devices available".to_string())
                    } else {
                        print!(
                            "Select the device event number [{}-{}]: ",
                            mapping.range(..).next().unwrap().0,
                            mapping.range(..).next_back().unwrap().0,
                        );
                        io::stdout().flush().map_err(|error| error.to_string())?;

                        let event_number: Result<u16, _> = try_read!();
                        match event_number {
                            Err(error) => Err(error.to_string()),
                            Ok(event_number) => {
                                let map = mapping.get(&event_number);
                                match map {
                                    None => Err("No corresponding device could be found.".to_string()),
                                    Some(map) => {
                                        *entered_device_name = Some(map.2.clone());

                                        Ok(Ok(map.0.clone()))
                                    },
                                }
                            },
                        }
                    }
                },
            }
        }
    }
}
