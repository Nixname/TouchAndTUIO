use std::{collections::HashMap, sync::mpsc, thread};

use arguments::Arguments;
use clap::Parser;
use network::receive_tuio;
use touch_emulation::emulate_touch;
use tuio::mouse_data::CursorData;

use crate::arguments::initialise_arguments;

extern crate uinput_tokio;

mod arguments;
mod iterators;
mod network;
mod touch_emulation;

#[cfg(test)]
mod tests;

#[tokio::main]
async fn main() -> Result<(), String> {
    let arguments = Arguments::parse();

    initialise_arguments(arguments)?;

    let (sender, receiver) = mpsc::channel::<HashMap<u16, CursorData>>();

    let join_handle = {
        thread::spawn(|| -> Result<(), String> {
            receive_tuio(sender)?;

            Ok(())
        })
    };

    emulate_touch(receiver).await?;

    join_handle
        .join()
        .expect("Could not join the networking thread.")?;

    Ok(())
}
