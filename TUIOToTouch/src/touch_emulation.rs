use helpers::check_access_to_file;
use itertools::Itertools;
use multi_map::MultiMap;
use std::collections::hash_map::RandomState;
use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;
use std::path::Path;
use std::sync::mpsc::Receiver;
use std::thread;
use std::time::Duration;
use tuio::mouse_data::CursorData;
use uinput_tokio::event::absolute::Multi;
use uinput_tokio::{Device, Event};

use crate::arguments::get_arguments;
use crate::iterators::SlotFinder;

pub async fn emulate_touch(receiver: Receiver<HashMap<u16, CursorData>>) -> Result<(), String> {
    let arguments = get_arguments().map_err(|_| "Could not read the arguments".to_string())?;

    const X_MIN: i32 = 0;
    const X_MAX: i32 = 4095;
    const Y_MIN: i32 = 0;
    const Y_MAX: i32 = 4095;

    const X_SPAN: i32 = X_MAX - X_MIN;
    const Y_SPAN: i32 = Y_MAX - Y_MIN;

    let mut device = match uinput_tokio::open(Path::new(&arguments.uinput_path)) {
        Ok(builder) => builder,
        Err(uinput_tokio::Error::Nix(nix::Error::Sys(nix::errno::Errno::EACCES))) => {
            check_access_to_file(Path::new("/dev/uinput"), None)?;

            return Err("Insufficient privileges to create the touch emulation device. Execute this command with super user privileges.".to_owned());
        }
        Err(uinput_tokio::Error::Nix(nix::Error::Sys(nix::errno::Errno::ENOENT))) => {
            return Err(format!("The path specified to interact with the uinput Kernel module '{}' does not exist.", &arguments.uinput_path));
        },
        Err(error) => {
            return Err(format!(
            "Received unexpected error when trying to create the touch emulation device.\nError: {:#?}",
            error
        ))
        }
    };

    device = device
        .name(&arguments.device_name)
        .unwrap()
        .bus(arguments.device_bus)
        .vendor(arguments.device_vendor)
        .product(arguments.device_product)
        .version(arguments.device_version)
        .event(uinput_tokio::event::Absolute::Multi(Multi::Slot))
        .unwrap()
        .min(0)
        .max(100)
        .event(uinput_tokio::event::Absolute::Multi(Multi::TrackingId))
        .unwrap()
        .min(0)
        .max(65535)
        .event(uinput_tokio::event::Absolute::Multi(Multi::PositionX))
        .unwrap()
        .min(X_MIN)
        .max(X_MAX)
        .fuzz(4)
        .event(uinput_tokio::event::Absolute::Multi(Multi::PositionY))
        .unwrap()
        .min(Y_MIN)
        .max(Y_MAX)
        .fuzz(4)
        .event(uinput_tokio::event::Absolute::Position(
            uinput_tokio::event::absolute::Position::X,
        ))
        .unwrap()
        .min(X_MIN)
        .max(X_MAX)
        .event(uinput_tokio::event::Absolute::Position(
            uinput_tokio::event::absolute::Position::Y,
        ))
        .unwrap()
        .min(Y_MIN)
        .max(Y_MAX)
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::Touch,
        )))
        .unwrap()
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::Finger,
        )))
        .unwrap()
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::DoubleTap,
        )))
        .unwrap()
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::TripleTap,
        )))
        .unwrap()
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::QuadTap,
        )))
        .unwrap()
        .event(Event::Controller(uinput_tokio::event::Controller::Digi(
            uinput_tokio::event::controller::Digi::QuintTap,
        )))
        .unwrap()
        .set_property(match arguments.touchpad_mode {
            false => &uinput_tokio::device::builder::InputDeviceProperty::InputPropDirect,
            true => &uinput_tokio::device::builder::InputDeviceProperty::InputPropPointer,
        })
        .unwrap();

    let mut device = device.create().await.unwrap();

    // Wait for the system to initialize and adjust for the new device
    match arguments.delay_ms_after_creation {
        0 => {}
        1.. => {
            thread::sleep(Duration::from_millis(
                arguments.delay_ms_after_creation.into(),
            ));
        }
    }

    #[derive(Debug)]
    struct Cursor {
        pub x: i32,
        pub y: i32,
        pub new: bool,
    }

    #[derive(Debug)]
    struct EventData {
        /// First key: session ID
        /// Second key: slot index
        pub cursors: MultiMap<u16, u16, Cursor>,
        pub abs_x: i32,
        pub abs_y: i32,
        pub touch: bool,
        pub finger_count: Option<FingerCount>,
    }
    #[derive(Debug, PartialEq, Eq)]
    enum FingerCount {
        Finger,
        DoubleTap,
        TripleTap,
        QuadTap,
        QuintTap,
    }
    impl FingerCount {
        pub fn get_code(&self) -> uinput_tokio::event::controller::Digi {
            match *self {
                FingerCount::Finger => uinput_tokio::event::controller::Digi::Finger,
                FingerCount::DoubleTap => uinput_tokio::event::controller::Digi::DoubleTap,
                FingerCount::TripleTap => uinput_tokio::event::controller::Digi::TripleTap,
                FingerCount::QuadTap => uinput_tokio::event::controller::Digi::QuadTap,
                FingerCount::QuintTap => uinput_tokio::event::controller::Digi::QuintTap,
            }
        }
    }

    let mut current_slot = 0;
    let mut current_primary_slot: Option<u16> = None;
    let mut previous_event_data: Option<EventData> = None;

    while let Ok(cursors_data) = receiver.recv() {
        fn calc_cursor(cursor_data: &CursorData, new: bool) -> Cursor {
            let x = (cursor_data.x * (X_SPAN as f32)) as i32 + X_MIN;
            let y = (cursor_data.y * (Y_SPAN as f32)) as i32 + Y_MIN;

            Cursor { x, y, new }
        }

        async fn set_slot(device: &mut Device, current_slot: &mut i32, slot_id: u16) {
            device.send(Multi::Slot, slot_id as i32).await.unwrap();
            *current_slot = slot_id as i32;
        }

        async fn clear_slot(device: &mut Device, current_slot: &mut i32, slot_id: u16) {
            set_slot(device, current_slot, slot_id).await;
            device.send(Multi::TrackingId, -1).await.unwrap();
        }

        // Contains the slot numbers
        let mut lost_cursors = Vec::<u16>::new();

        let cursors: MultiMap<u16, u16, Cursor> = match &previous_event_data {
            None => {
                let mut cursors = MultiMap::new();

                for (index, (session_id, cursor_data)) in cursors_data.iter().enumerate() {
                    cursors.insert(*session_id, index as u16, calc_cursor(cursor_data, true));
                }

                cursors
            }
            Some(previous_event_data) => {
                let mut cursors = MultiMap::new();

                let previous_session_ids_set = HashSet::<u16, RandomState>::from_iter(
                    previous_event_data
                        .cursors
                        .iter()
                        .map(|(session_id, _)| *session_id),
                );
                let next_session_ids_set =
                    HashSet::<u16, RandomState>::from_iter(cursors_data.keys().copied());

                for lost_session_id in previous_session_ids_set.difference(&next_session_ids_set) {
                    lost_cursors.push(
                        *previous_event_data
                            .cursors
                            .get_key(lost_session_id)
                            .unwrap(),
                    );
                }

                for retained_session_id in
                    next_session_ids_set.intersection(&previous_session_ids_set)
                {
                    let slot_index = previous_event_data
                        .cursors
                        .get_key(retained_session_id)
                        .unwrap();
                    let cursor_data = cursors_data.get(retained_session_id).unwrap();

                    cursors.insert(
                        *retained_session_id,
                        *slot_index,
                        calc_cursor(cursor_data, false),
                    );
                }

                let mut slot_finder =
                    SlotFinder::new(previous_event_data.cursors.keys_alt().sorted().peekable());

                for new_session_id in next_session_ids_set.difference(&previous_session_ids_set) {
                    let cursor_data = cursors_data.get(new_session_id).unwrap();
                    let next_slot = slot_finder.next().unwrap();
                    let cursor = calc_cursor(cursor_data, true);

                    cursors.insert(*new_session_id, next_slot, cursor);
                }

                cursors
            }
        };

        let mut cursors_sorted = cursors
            .iter()
            .sorted_by(|a, b| Ord::cmp(&a.1 .0, &b.1 .0))
            .peekable();

        if let Some(current_primary_slot_value) = current_primary_slot {
            if !cursors.contains_key_alt(&current_primary_slot_value) {
                current_primary_slot = None;
            }
        }

        if current_primary_slot.is_none() {
            current_primary_slot = cursors_sorted.peek().map(|(_, (slot, _))| *slot);
        }

        let (abs_x, abs_y) = if let Some(current_primary_slot) = current_primary_slot {
            let cursor = cursors.get_alt(&current_primary_slot).unwrap();

            (cursor.x, cursor.y)
        } else if let Some(previous_event_data) = &previous_event_data {
            (previous_event_data.abs_x, previous_event_data.abs_y)
        } else {
            (X_MIN, Y_MIN)
        };

        let touch = !cursors.is_empty();

        let finger_count = match cursors.len() {
            0 => None,
            1 => Some(FingerCount::Finger),
            2 => Some(FingerCount::DoubleTap),
            3 => Some(FingerCount::TripleTap),
            4 => Some(FingerCount::QuadTap),
            5 => Some(FingerCount::QuintTap),
            _ => Some(FingerCount::QuintTap),
        };

        let current_event_data = EventData {
            abs_x,
            abs_y,
            cursors,
            touch,
            finger_count,
        };

        {
            // Act

            for lost_cursor in lost_cursors {
                clear_slot(&mut device, &mut current_slot, lost_cursor).await;
            }

            for (tracking_id, (slot_id, cursor)) in current_event_data.cursors.iter() {
                set_slot(&mut device, &mut current_slot, *slot_id).await;
                if cursor.new {
                    device
                        .send(Multi::TrackingId, *tracking_id as i32)
                        .await
                        .unwrap();
                }

                let previous_cursor = previous_event_data
                    .as_ref()
                    .map(|previous_event_data| previous_event_data.cursors.get(tracking_id))
                    .flatten();

                match previous_cursor {
                    None => {
                        device.send(Multi::PositionX, cursor.x).await.unwrap();
                        device.send(Multi::PositionY, cursor.y).await.unwrap();
                    }
                    Some(previous_cursor) => {
                        if previous_cursor.x != cursor.x {
                            device.send(Multi::PositionX, cursor.x).await.unwrap();
                        }
                        if previous_cursor.y != cursor.y {
                            device.send(Multi::PositionY, cursor.y).await.unwrap();
                        }
                    }
                }
            }

            device
                .send(
                    uinput_tokio::event::absolute::Position::X,
                    current_event_data.abs_x,
                )
                .await
                .unwrap();
            device
                .send(
                    uinput_tokio::event::absolute::Position::Y,
                    current_event_data.abs_y,
                )
                .await
                .unwrap();

            device
                .send(
                    uinput_tokio::event::controller::Digi::Touch,
                    if current_event_data.touch { 1 } else { 0 },
                )
                .await
                .unwrap();

            match &previous_event_data
                .map(|previous_event_data| previous_event_data.finger_count)
                .flatten()
            {
                None => match &current_event_data.finger_count {
                    None => {}
                    Some(finger_count) => {
                        device.send(finger_count.get_code(), 1).await.unwrap();
                    }
                },
                Some(previous_finger_count) => match &current_event_data.finger_count {
                    None => {
                        device
                            .send(previous_finger_count.get_code(), 0)
                            .await
                            .unwrap();
                    }
                    Some(finger_count) => {
                        if finger_count != previous_finger_count {
                            device
                                .send(previous_finger_count.get_code(), 0)
                                .await
                                .unwrap();
                            device.send(finger_count.get_code(), 1).await.unwrap();
                        }
                    }
                },
            }

            device.synchronize().await.unwrap();
        }

        previous_event_data = Some(current_event_data);
    }

    Ok(())
}
