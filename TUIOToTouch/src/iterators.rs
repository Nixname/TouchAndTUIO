use std::iter::Peekable;

pub struct SlotFinder<'a, I: Iterator<Item = &'a u16>> {
    occupied_slots: Peekable<I>,
    last_taken: Option<u16>,
}

impl<'a, I: Iterator<Item = &'a u16>> SlotFinder<'a, I> {
    /// Assumes it is being passed an iterator yielding elements in ascending order
    pub fn new(occupied_slots: Peekable<I>) -> Self {
        Self {
            occupied_slots,
            last_taken: None,
        }
    }
}

impl<'a, I: Iterator<Item = &'a u16>> Iterator for SlotFinder<'a, I> {
    type Item = u16;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let next_to_take = self.last_taken.map(|v| v + 1).unwrap_or(0);

            match self.occupied_slots.peek() {
                Some(occupied) => {
                    self.last_taken = Some(next_to_take);

                    if **occupied == next_to_take {
                        self.occupied_slots.next();
                        continue;
                    } else {
                        return Some(next_to_take);
                    }
                }
                None => {
                    self.occupied_slots.next();
                    self.last_taken = Some(next_to_take);
                    return self.last_taken;
                }
            }
        }
    }
}
