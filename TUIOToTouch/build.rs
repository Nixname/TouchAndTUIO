use std::{env, fs::File, io::Write};

fn write_env(file: &mut File, variable_name: &str) -> Result<(), String> {
    file.write(
        format!(
            "{}\n{}\n\n",
            variable_name,
            env::var(variable_name).map_err(|error| error.to_string())?
        )
        .as_bytes(),
    )
    .map_err(|error| error.to_string())?;

    Ok(())
}

fn main() -> Result<(), String> {
    let mut file = File::create(".variables").map_err(|error| error.to_string())?;

    write_env(&mut file, "CARGO_PKG_NAME")?;
    write_env(&mut file, "CARGO_PKG_VERSION")?;
    write_env(&mut file, "CARGO_CFG_TARGET_ARCH")?;
    write_env(&mut file, "TARGET")?;
    write_env(&mut file, "PROFILE")?;

    Ok(())
}
