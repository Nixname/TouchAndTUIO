use crossbeam_channel::RecvTimeoutError;
use tuio::messages::get_cursor_bundle;
use tuio::messages::get_keep_alive_bundle;
use tuio::messages::TuioBundle;

use std::collections::HashMap;

use std::net::UdpSocket;

use std::sync::RwLockReadGuard;
use std::thread;
use std::time::Duration;
use std::time::SystemTime;

use rosc::encoder;

use rosc::OscPacket;
use tuio::mouse_data::ComputedMouseData;
use tuio::mouse_data::MouseData;

use crate::variables::get_constants;
use crate::variables::Constants;
use crate::variables::FSEQ;

pub fn spawn_tuio_network_thread(
    mouse_data_receiver: crossbeam_channel::Receiver<MouseData>,
) -> thread::JoinHandle<()> {
    thread::spawn(move || {
        let constants = get_constants().unwrap();

        let socket =
            UdpSocket::bind((constants.source_address.as_str(), constants.source_port)).unwrap();

        let local_socket_address = socket
            .local_addr()
            .expect("Could not fetch the local socket address.");

        let mut previous_mouse_data: Option<ComputedMouseData> = None;

        fn send_tuio_bundle(
            socket: &UdpSocket,
            constants: &RwLockReadGuard<Constants>,
            tuio_bundle: &TuioBundle,
        ) {
            let packet = OscPacket::Bundle(tuio_bundle.to_osc());
            let data = encoder::encode(&packet).unwrap();

            if let Err(error) = socket.send_to(
                &data,
                (
                    constants.destination_address.as_str(),
                    constants.destination_port,
                ),
            ) {
                println!("Could not send TUIO bundle: {:#?}", error);
            }
        }

        loop {
            let mouse_data: MouseData = if constants.keep_alive_interval_in_ms > 0 {
                match mouse_data_receiver.recv_timeout(Duration::from_millis(
                    constants.keep_alive_interval_in_ms as u64,
                )) {
                    Err(recv_timeout_error) => match recv_timeout_error {
                        RecvTimeoutError::Timeout => {
                            let mouse_data = match &previous_mouse_data {
                                Some(previous_mouse_data) => previous_mouse_data.to_mouse_data(),
                                None => MouseData {
                                    timestamp: SystemTime::now(),
                                    cursors: HashMap::new(),
                                },
                            };

                            let tuio_bundle = get_keep_alive_bundle(
                                &constants.address_prefix,
                                &constants.source_identifier,
                                &mouse_data,
                                &local_socket_address,
                            );

                            send_tuio_bundle(&socket, &constants, &tuio_bundle);

                            continue;
                        }
                        RecvTimeoutError::Disconnected => break,
                    },
                    Ok(mouse_data) => mouse_data,
                }
            } else {
                match mouse_data_receiver.recv() {
                    Err(_) => break,
                    Ok(mouse_data) => mouse_data,
                }
            };

            let (computed_mouse_data, tuio_bundle) = get_cursor_bundle(
                &constants.address_prefix,
                &constants.source_identifier,
                &FSEQ,
                &mouse_data,
                &previous_mouse_data,
                &local_socket_address,
            );

            previous_mouse_data = Some(computed_mouse_data);

            send_tuio_bundle(&socket, &constants, &tuio_bundle);
        }
    })
}
