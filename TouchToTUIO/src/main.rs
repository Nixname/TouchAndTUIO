mod arguments;
mod event;
mod mouse_reader;
mod tuio_sender;
mod variables;

use crate::event::display_devices;
use crate::mouse_reader::*;
use crate::tuio_sender::*;

use std::path::PathBuf;
use std::sync::mpsc::channel;
use std::time::Duration;

use arguments::Arguments;
use clap::Parser;
use event::get_event_path;
use ignore_result::Ignore;
use notify::watcher;
use notify::DebouncedEvent;
use notify::RecursiveMode;
use notify::Watcher;
use tuio::mouse_data::MouseData;

fn main() -> Result<(), String> {
    let constants = variables::initialise_constants(Arguments::parse())?;

    if constants.list_devices {
        let mapping = get_event_name_mapping()?;

        display_devices(&mapping);

        return Ok(());
    }

    let join_handle = {
        let (sender, receiver) = crossbeam_channel::unbounded::<MouseData>();

        let join_handle = spawn_tuio_network_thread(receiver);

        let mut entered_device_name = None;

        match constants.watch {
            false => {
                let event_path = get_event_path(&mut entered_device_name)??;

                read_mouse_data(sender, &event_path)?;
            }
            true => loop {
                let event_path = match {
                    let mut event_path: Option<PathBuf> = None;
                    let (tx, rx) = channel();

                    let mut watcher =
                        watcher(tx, Duration::from_secs(5)).map_err(|error| error.to_string())?;

                    watcher
                        .watch("/dev/input", RecursiveMode::NonRecursive)
                        .map_err(|error| error.to_string())?;

                    if let Ok(determined_event_path) = get_event_path(&mut entered_device_name)? {
                        if determined_event_path.exists() {
                            event_path = Some(determined_event_path);
                        }
                    }

                    if event_path.is_none() {
                        loop {
                            if let DebouncedEvent::Create(_) =
                                rx.recv().map_err(|error| error.to_string())?
                            {
                                if let Ok(determined_event_path) =
                                    get_event_path(&mut entered_device_name)?
                                {
                                    if !determined_event_path.exists() {
                                        continue;
                                    }

                                    event_path = Some(determined_event_path);

                                    break;
                                }
                            }
                        }
                    }

                    event_path
                } {
                    Some(event_path) => event_path,
                    None => return Err("The event path is still unset.".to_string()),
                };

                read_mouse_data(sender.clone(), &event_path).ignore();
            },
        }

        join_handle
    };

    join_handle.join().unwrap();

    Ok(())
}
