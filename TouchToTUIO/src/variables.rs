use std::sync::{atomic, RwLock, RwLockReadGuard};

use tuio::types::SourceIdentifier;

use crate::arguments::Arguments;

pub static FSEQ: atomic::AtomicU16 = atomic::AtomicU16::new(1);

#[derive(Debug, Eq, PartialEq)]
pub enum Rotation {
    Top,
    Right,
    Bottom,
    Left,
}

#[derive(Debug)]
pub struct Constants {
    pub event_path: Option<String>,
    pub device_name: Option<String>,
    pub source_address: String,
    pub source_port: u16,
    pub destination_address: String,
    pub destination_port: u16,
    pub source_identifier: Option<SourceIdentifier>,
    pub address_prefix: String,
    pub block_event: bool,
    pub list_devices: bool,
    pub non_interactive: bool,
    pub mirror_x: bool,
    pub mirror_y: bool,
    pub rotation: Rotation,
    pub watch: bool,
    pub keep_alive_interval_in_ms: u16,
}

lazy_static::lazy_static! {
    static ref CONSTANTS: RwLock<Constants> = RwLock::new(Constants {
        event_path: None,
        device_name: None,
        source_address: "".to_string(),
        source_port: 0,
        destination_address: "".to_string(),
        destination_port: 0,
        source_identifier: None,
        address_prefix: "".to_string(),
        block_event: false,
        list_devices: false,
        non_interactive: false,
        mirror_x: false,
        mirror_y: false,
        rotation: Rotation::Top,
        watch: false,
        keep_alive_interval_in_ms: 0,
    });

    static ref CONSTANTS_INITIALISED: RwLock<bool> = RwLock::new(false);
}

pub fn initialise_constants<'a>(
    input: Arguments,
) -> Result<RwLockReadGuard<'a, Constants>, String> {
    {
        let mut constants = CONSTANTS
            .write()
            .map_err(|_| "The constants are poisoned.".to_string())?;

        let mut constants_initialised = CONSTANTS_INITIALISED
            .write()
            .map_err(|_| "constants_initialised is poisoned.".to_string())?;

        match *constants_initialised {
            true => {
                return Err("The constants were already set.".to_string());
            }
            false => {
                *constants_initialised = true;
            }
        }

        match input.list_devices {
            false => {
                if input.event_path.is_none() && input.device_name.is_none() {
                    if input.non_interactive {
                        return Err(
                            "Neither an event path nor a device name was supplied.".to_string()
                        );
                    }
                } else if input.event_path.is_some() && input.device_name.is_some() {
                    return Err("Both an event name and a device name was supplied.".to_string());
                }
            }
            true => {
                if input.event_path.is_some() || input.device_name.is_some() {
                    return Err(
                        "Ordered to both list devices and read from a specific device.".to_string(),
                    );
                }
            }
        }

        let destination_address = input.destination_address;
        let destination_port = input.destination_port;
        let source_identifier = if input.source_identifier.is_empty() {
            None
        } else {
            Some(SourceIdentifier::new(input.source_identifier)?)
        };

        let rotation = {
            let number_of_rotations = input.number_of_rotations % 4;

            match number_of_rotations {
                0 => Rotation::Top,
                1 => Rotation::Right,
                2 => Rotation::Bottom,
                3 => Rotation::Left,
                4.. => {
                    return Err(format!(
                        "Received an unexpected number of rotations: {}",
                        number_of_rotations
                    ))
                }
            }
        };

        *constants = Constants {
            source_identifier,
            device_name: input.device_name,
            event_path: input.event_path,
            source_address: input.source_address,
            source_port: input.source_port,
            destination_address,
            destination_port,
            address_prefix: input.addres_prefix,
            block_event: input.block_event,
            list_devices: input.list_devices,
            non_interactive: input.non_interactive,
            mirror_x: input.mirror_x,
            mirror_y: input.mirror_y,
            rotation,
            watch: input.watch,
            keep_alive_interval_in_ms: input.keep_alive_interval_in_ms,
        };
    }

    CONSTANTS
        .read()
        .map_err(|_| "The constants are poisoned.".to_string())
}

pub fn get_constants<'a>() -> Result<RwLockReadGuard<'a, Constants>, ()> {
    CONSTANTS.read().map_err(|_| ())
}
